
# FR test

## Run Locally

Clone the project

```bash
git clone https://gitlab.com/lehazavalin95/fr_test
```

Go to the project directory

```bash
cd fr_test
```

Copy .env.example

```bash
cp .env.example .env
```

Fill SECRET_KEY, FBRQ_API_ROOT, FBRQ_API_TOKEN

Run docker

```bash
docker-compose up -d
```

Migrate

```bash
docker exec -t <web container id> python manage.py migrate
```

Create superuser

```bash
docker exec -it <web container id> python manage.py createsuperuser
```

Copy ***token*** /admin/authtoken/tokenproxy/

## API Reference

You must add the token to the headers of each request

Authorization: Token ***token***

#### root

```http
  GET /api
```

#### api docs drf

```http
  GET /api/*
```

#### api docs swagger

```http
  GET /api/docs
```

## Running Tests

To run tests, run the following command

```bash
  docker exec -t fr_test-web-1 python manage.py test
```
