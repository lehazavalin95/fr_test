from django.urls import include, path
from django.views.generic import TemplateView

from rest_framework import routers
from rest_framework.authtoken import views as rest_views

from main.api import views


router = routers.DefaultRouter()
router.register(r'operator_code', views.OperatorCodeViewSet)
router.register(r'tag', views.TagViewSet)
router.register(r'mailing_lists', views.MailingListViewSet)
router.register(r'clients', views.ClientViewSet)
router.register(r'messages', views.MessageViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls',
                              namespace='rest_framework')),
    path('api-token-auth/', rest_views.obtain_auth_token),
    path('docs/', TemplateView.as_view(
        template_name='swagger.html',
        extra_context={'schema_url':'openapi-schema'}
    ), name='api-docs'),
]
