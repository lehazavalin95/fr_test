from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

import zoneinfo


class Tag(models.Model):
    '''
    Тэг
    '''
    tag = models.CharField(
        verbose_name=_('Тэг'),
        max_length=25,
        unique=True,
        null=False
    )

    class Meta:
        verbose_name = _('Тэг')
        verbose_name_plural = _('Тэги')

    def __str__(self):
        return self.tag


class OperatorCode(models.Model):
    '''
    Код мобильного оператора
    '''
    code = models.CharField(
        verbose_name=_('Код мобильного оператора'),
        max_length=3,
        unique=True,
        null=False
    )

    class Meta:
        verbose_name = _('Код оператор')
        verbose_name_plural = _('Коды операторов')

    def __str__(self):
        return self.code


class MailingList(models.Model):
    '''
    Рассылка
    '''
    datetime_start = models.DateTimeField(
        verbose_name=_('Дата и время запуска рассылки')
    )
    datetime_end = models.DateTimeField(
        verbose_name=_('Дата и время окончания рассылки')
    )
    message_text = models.TextField(
        verbose_name=_('Текст сообщения')
    )
    operator_codes = models.ManyToManyField(
        to=OperatorCode,
        verbose_name=_('Коды операторов'),
        related_name='operator_codes'
    )
    tags = models.ManyToManyField(
        to=Tag,
        verbose_name=_('Тэги'),
        related_name='tags'
    )

    class Meta:
        verbose_name = _('Рассылка')
        verbose_name_plural = _('Рассылки')

    def __str__(self):
        return f'Рассылка {self.pk} {self.datetime_start} - {self.datetime_end}'


class Client(models.Model):
    '''
    Клиент
    '''
    TIME_ZONES = tuple(
        map(lambda i: i, enumerate(zoneinfo.available_timezones()))
    )
    phone_number = models.DecimalField(
        verbose_name=_('Номер телефона'),
        max_digits=11,
        decimal_places=0,
        help_text=_('Формат: 7XXXXXXXXXX'),
        unique=True
    )
    time_zone = models.PositiveSmallIntegerField(
        choices=TIME_ZONES
    )
    tags = models.ManyToManyField(
        to=Tag,
        verbose_name=_('Тэги'),
        related_name='client_tags'
    )
    operator_code = models.ForeignKey(
        to=OperatorCode,
        on_delete=models.SET_DEFAULT,
        default=None,
        verbose_name=_('Код мобильного оператора'),
        related_name='codes'
    )

    class Meta:
        verbose_name = _('Клиент')
        verbose_name_plural = _('Клиенты')

    def __str__(self):
        return str(self.phone_number)


class Message(models.Model):
    '''
    Сообщение
    '''
    date_create = models.DateTimeField(
        verbose_name=_('Дата и время создания'),
        auto_now=True
    )
    sent = models.BooleanField(
        verbose_name=_('Отправлено'),
        default=False
    )
    mailing_list = models.ForeignKey(
        to=MailingList,
        verbose_name=_('Рассылка'),
        related_name='mailing_list',
        on_delete=models.SET_NULL,
        null=True
    )
    client = models.ForeignKey(
        to=Client,
        verbose_name=_('Клиент'),
        related_name='client',
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = _('Сообщение'),
        verbose_name_plural = _('Сообщения')
        unique_together = ('client', 'mailing_list')

    def __str__(self) -> str:
        return f'{str(self.client.phone_number)} {self.mailing_list.__str__()}'

    @property
    def available_to_send(self):
        return self.mailing_list.datetime_start < timezone.now() <  self.mailing_list.datetime_end
