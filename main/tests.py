from rest_framework import status
from rest_framework.test import APITestCase, RequestsClient


class ApiTestCase(APITestCase):
    def setUp(self):
        self.client = RequestsClient()
        self.root = 'http://testserver/api/'

    def test_root_unauthenticated(self):
        response = self.client.get(self.root)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


class OperatorCodeTestCase(APITestCase):
    def setUp(self):
        self.client = RequestsClient()
        self.root = 'http://testserver/api/operator_code/'

    def test_unauthenticated(self):
        response = self.client.get(self.root)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


class TagTestCase(APITestCase):
    def setUp(self):
        self.client = RequestsClient()
        self.root = 'http://testserver/api/tag/'

    def test_unauthenticated(self):
        response = self.client.get(self.root)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

class MailingListTestCase(APITestCase):
    def setUp(self):
        self.client = RequestsClient()
        self.root = 'http://testserver/api/mailing_lists/'

    def test_unauthenticated(self):
        response = self.client.get(self.root)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


class ClientTestCase(APITestCase):
    def setUp(self):
        self.client = RequestsClient()
        self.root = 'http://testserver/api/clients/'

    def test_unauthenticated(self):
        response = self.client.get(self.root)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


class MessageTestCase(APITestCase):
    def setUp(self):
        self.client = RequestsClient()
        self.root = 'http://testserver/api/messages/'

    def test_root(self):
        response = self.client.get(self.root)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
