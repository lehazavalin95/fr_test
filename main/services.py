import requests
import json

from django.conf import settings


class FbrqAPI:
    def __init__(self):
        self.root = settings.API_ROOT
        self.token = settings.API_TOKEN
        self.headers = {
            'Authorization': f'Bearer {self.token}',
            'Content-Type': 'application/json'
        }

    def send(self, msg_id: int, phone:str, msg_text:str) -> bool:
        '''Send message'''
        url = self.root + '/send/' + str(msg_id)
        data = {
            'id': msg_id,
            'phone': phone,
            'text': msg_text
        }
        try:
            r = requests.post(url, json.dumps(data), headers=self.headers)
            if r.status_code == 200:
                return True
        except (requests.exceptions.Timeout, requests.exceptions.ConnectionError):
            return False
