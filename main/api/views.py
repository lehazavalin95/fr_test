from rest_framework import viewsets

from main.models import MailingList, Client, Message, OperatorCode, Tag
from main.api import serializers


class OperatorCodeViewSet(viewsets.ModelViewSet):
    queryset = OperatorCode.objects.all()
    serializer_class = serializers.OperatorCodeSerializer


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = serializers.TagSerializer


class MailingListViewSet(viewsets.ModelViewSet):
    queryset = MailingList.objects.all()
    serializer_class = serializers.MailingListSerializer


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = serializers.ClientSerializer


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = serializers.MessageSerializer
