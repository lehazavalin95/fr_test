from main import models
from rest_framework import serializers


class OperatorCodeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.OperatorCode
        fields = ['code']


class TagSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Tag
        fields = ['tag']


class MailingListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.MailingList
        fields = ['datetime_start', 'datetime_end', 
        'message_text', 'operator_codes', 'tags']


class ClientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Client
        fields = ['phone_number', 'time_zone', 'tags', 'operator_code']


class MessageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Message
        fields = ['date_create', 'sent', 'client', 'mailing_list']
