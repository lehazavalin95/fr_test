from django.contrib import admin

from main import models


@admin.register(models.Tag)
class TagAdmin(admin.ModelAdmin):
    search_fields = ('tag', )


@admin.register(models.OperatorCode)
class OperatorCodeAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('phone_number', 'time_zone', 'operator_code')
    list_filter = ('operator_code', )
    search_fields = ('phone_number', )


@admin.register(models.MailingList)
class MailingListAdmin(admin.ModelAdmin):
    list_display = ('datetime_start', 'datetime_end', 'message_text')
    list_filter = ('operator_codes', )
    search_fields = ('message_text', )


@admin.register(models.Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('date_create', 'client', 'mailing_list', 'sent')
    list_filter = ('sent', )
    search_fields = ('client__phone_number', )
