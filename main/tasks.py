import logging

from django.utils import timezone
from django.db.utils import IntegrityError

from celery import shared_task

from .models import MailingList, Message, Client
from .services import FbrqAPI


logger = logging.getLogger(__name__)


@shared_task
def send_messages():
    '''
    Отправка всех неотправленных сообщений, которые доступны для отправки
    '''
    for message in Message.objects.filter(sent=False):
        if message.available_to_send:
            send_message.delay(message.pk)


@shared_task
def send_message(message: int) -> None:
    '''
    Отправка сообщения
    '''
    fbrq = FbrqAPI()
    try:
        message = Message.objects.get(pk=message)
        sent = fbrq.send(
            message.pk, 
            str(message.client.phone_number), 
            message.mailing_list.message_text
        )
        if sent:
            message.sent = True
            message.save()
            logger.info(f'Сообщение {message.pk} отправлено')
        else:
            logger.error(f'Сообщение {message.pk} не отправлено')
    except Message.DoesNotExist:
        logger.error('Сообщение не найдено')


@shared_task
def create_messages() -> None:
    '''
    Создание очереди сообщений
    '''
    now = timezone.now()
    mls = MailingList.objects.filter(
        datetime_start__lte=now,
        datetime_end__gt=now
    )
    for ml in mls:
        clients = Client.objects.filter(
            tags__in=ml.tags.all(),
            operator_code__in=ml.operator_codes.all()
        )
        for client in clients:
            create_message.delay(client.pk, ml.pk)


@shared_task
def create_message(client: int, mailing_list: int) -> None:
    '''
    Создание сообщения для последуюей отправки
    '''
    try:
        Message.objects.create(
            client_id=client,
            mailing_list_id=mailing_list
        )
    except IntegrityError:
        pass
